FROM node:alpine as builder
#build payment
WORKDIR /app/ui-one
COPY ./ui-one/package.json .
RUN yarn
COPY ./ui-one/ .
RUN yarn build

#build uw
WORKDIR /app/uw
COPY ./ui-two/package.json .
# COPY ./ui-two/package-lock.json .
RUN yarn
COPY ./ui-two/ .
RUN yarn build

FROM bitnami/nginx:1.21.3-debian-10-r10

COPY --from=builder /app/ui-one/build /app/auione/
COPY --from=builder /app/ui-two/build /app/uitwo/

# COPY working.conf /etc/nginx/conf.d/default.conf

COPY bitnami.conf /opt/bitnami/nginx/conf/server_blocks/working.conf

# RUN mkdir config
# WORKDIR /app/config
# COPY ./env-config.js .

# WORKDIR /app
# COPY ./env-config.js .

# COPY ./startup.sh /opt/bitnami/scripts/nginx/

EXPOSE 8080

# Start Nginx server
#CMD ["/bin/bash", "-c", "/usr/share/nginx/payment/html/env.sh && nginx -g \"daemon off;\""]
#  CMD ["/bin/bash", "-c", "nginx -g \"daemon off;\""]
# ENTRYPOINT ["/docker-entrypoint.sh"]
# CMD ["/bin/bash", "-c", "nginx -g \"daemon off;\""]
CMD ["nginx", "-g", "daemon off;"]